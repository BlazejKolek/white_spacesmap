﻿using Ninject.Modules;
using WhiteSpaceMap.Business.Interfaces;
using WhiteSpaceMap.Business.Services;

namespace WhiteSpaceMap.Business
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IClientService>().To<ClientService>();
        }
    }
}
