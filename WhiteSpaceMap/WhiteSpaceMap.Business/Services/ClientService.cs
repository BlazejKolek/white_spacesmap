﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhiteSpaceMap.Business.Interfaces;
using WhiteSpaceMap.Business.Models;
using WhiteSpaceMap.Data;

namespace WhiteSpaceMap.Business.Services
{
   public class ClientService : IClientService
    {
        //private readonly IClientService _clientService;

        //public ClientService(IClientService clientService)
        //{
        //    _clientService = clientService;
        //}

        public IEnumerable<ClientBl> GetAllClients()
        {
            using (var context = new FirmEntities())
            {
                var mapperConf = new MapperConfiguration(cfg => {
                    cfg.CreateMap<Client, ClientBl>();
                });

                IMapper mapper = mapperConf.CreateMapper();
                //var clientsFormDB =  context.Clients.ToList();
                return  mapper.Map<List<Client>, List<ClientBl>>(context.Clients.ToList()); 
            } 
        }
    }
}
