﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhiteSpaceMap.Business.Models;
using WhiteSpaceMap.Data;

namespace WhiteSpaceMap.Business.Interfaces
{
   public interface IClientService
    {
        IEnumerable<ClientBl> GetAllClients();
    }
}
