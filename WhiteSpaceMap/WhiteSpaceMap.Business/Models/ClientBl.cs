﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSpaceMap.Business.Models
{
   public class ClientBl
    {
            public string Client_Id { get; set; }
            public string Name { get; set; }
            public string Powiat { get; set; }
            public string Województwo { get; set; }
            public string Zip { get; set; }
            public string Address { get; set; }
            public string City { get; set; }
            public string Full_Address { get; set; }
            public Nullable<decimal> Latitude { get; set; }
            public Nullable<decimal> Longitude { get; set; }
            public string Segment { get; set; }
    }
}
