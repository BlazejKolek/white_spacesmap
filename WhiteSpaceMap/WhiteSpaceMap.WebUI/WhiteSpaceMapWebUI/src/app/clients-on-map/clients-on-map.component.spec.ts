import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsOnMapComponent } from './clients-on-map.component';

describe('ClientsOnMapComponent', () => {
  let component: ClientsOnMapComponent;
  let fixture: ComponentFixture<ClientsOnMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsOnMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsOnMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
