import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Client} from 'src/models/client';
import { ClientService } from '../clientService';


@Component({
  selector: 'app-clients-on-map',
  templateUrl: './clients-on-map.component.html',
  styleUrls: ['./clients-on-map.component.css']
})
export class ClientsOnMapComponent implements OnInit {
  title = 'WhiteSpaceMapWebUI';
  lat : number = 52.237049;
  lng : number = 21.017532;

  clientList : Array<Client>;

  constructor(private client : ClientService){}
  
  ngOnInit(): void {
    this.getClients();
  }


  getClients(){
    this.client.getClients().subscribe(clients => this.clientList = clients) ;};

  
    getUrlMarker(segment:string): string {

      if (segment == 'A') {return 'http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png'}
       else if (segment == 'B') {return'http://maps.google.com/mapfiles/ms/icons/green-stars.png'}
       else if (segment == 'C') {return'http://maps.google.com/mapfiles/kml/paddle/orange-stars.png'}
       else if (segment == 'D') {return'http://maps.google.com/mapfiles/kml/paddle/grn-stars.png'}
       else if (segment == 'E') {return'http://maps.google.com/mapfiles/kml/paddle/pink-stars.png'}
       else if (segment == 'F') {return'http://maps.google.com/mapfiles/kml/paddle/ltblu-stars.png'}
       else return'http://maps.google.com/mapfiles/ms/icons/red-stars.png'
  
     }  

}

