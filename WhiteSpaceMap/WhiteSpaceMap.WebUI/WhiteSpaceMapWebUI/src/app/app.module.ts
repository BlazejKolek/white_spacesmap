import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout'
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule} from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ToastrModule } from 'ngx-toastr' ;

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientsOnMapComponent } from './clients-on-map/clients-on-map.component';
import { HttpClientModule } from '@angular/common/http';
import {ClientService} from 'src/app/clientService'
import { Routes, RouterModule } from '@angular/router';
import {AgmCoreModule} from '@agm/core';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component'

const appRoutes : Routes = [
            //  {path: '',redirectTo: 'app-root' , pathMatch: 'full'},
             {path:'',component:ClientsOnMapComponent,pathMatch:'full'},
            {path:'app-clients-on-map',component: ClientsOnMapComponent,pathMatch: 'full' }
          ]
const mapsKey : string = 'AIzaSyDX_AIfjpgpGzP3pV1naCX-JOfSgzNlygU' 

@NgModule({
  declarations: [
    AppComponent,
    ClientsOnMapComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    FlexLayoutModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    AgmCoreModule.forRoot({apiKey: mapsKey})
  ],
  providers: [
    ClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
