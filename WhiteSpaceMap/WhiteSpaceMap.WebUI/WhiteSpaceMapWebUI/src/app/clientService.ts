import { HttpClient } from '@angular/common/http';
import { Client } from 'src/models/client';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class ClientService {

    private url : string = '/api/Client';

    constructor(private http : HttpClient){}
    
      getClients() : Observable<Client[]> {
        return this.http.get<Client[]>(this.url);
       }
}