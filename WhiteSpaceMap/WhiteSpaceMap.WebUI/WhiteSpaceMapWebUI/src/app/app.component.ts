import { Component } from '@angular/core';
import { ClientService } from './clientService';
import { Client } from 'src/models/client';
import { google } from '@agm/core/services/google-maps-types';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'WhiteSpaceMapWebUI';
  lat : number = 52.237049;
  lng : number = 21.017532;
  

clientList : Array<Client> ;


constructor(private client : ClientService) {}



ngOnInit(){
  this.getClients();
}

getClients(){
   this.client.getClients().subscribe(clients => this.clientList = clients) ;};
    

   getUrlMarker(segment:string): string {

    if (segment == 'A') {return 'http://maps.google.com/mapfiles/kml/paddle/ylw-stars.png'}
     else if (segment == 'B') {return'http://maps.google.com/mapfiles/ms/icons/green-stars.png'}
     else if (segment == 'C') {return'http://maps.google.com/mapfiles/kml/paddle/orange-stars.png'}
     else if (segment == 'D') {return'http://maps.google.com/mapfiles/kml/paddle/grn-stars.png'}
     else if (segment == 'E') {return'http://maps.google.com/mapfiles/kml/paddle/pink-stars.png'}
     else if (segment == 'F') {return'http://maps.google.com/mapfiles/kml/paddle/ltblu-stars.png'}
     else return'http://maps.google.com/mapfiles/ms/icons/red-stars.png'

   }
}

