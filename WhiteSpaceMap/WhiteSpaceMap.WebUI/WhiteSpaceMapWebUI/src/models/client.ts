export class Client {

    public Client_Id : string;
    public Name : string;
    public Powiat : string;
    public Województwo : string;
    public Zip : string;
    public Address : string;
    public City : string;
    public Full_Address : string;
    public Latitude : number ;
    public Longitude : number ;
    public Segment : string ;
    public Marker : string;
}