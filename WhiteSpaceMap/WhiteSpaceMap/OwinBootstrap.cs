﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSpaceMap.WebApi
{
   internal class OwinBootstrap
    {
        private readonly IAppSettings _appSettings = new AppSettings();
        private IDisposable _webApi;

        public void Start()
        {
            var baseAdress = _appSettings.WebApiUri;
            _webApi = WebApp.Start<Startup>(baseAdress);
        }

        public void Stop()
        {
            _webApi.Dispose();
        }


    }
}
