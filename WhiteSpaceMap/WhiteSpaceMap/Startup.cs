﻿using Owin;
using System.Web.Http;

using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;

namespace WhiteSpaceMap.WebApi
{
   public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );

            appBuilder
                .UseNinjectMiddleware(new NinjectBootstrap().GetKernel)
                .UseNinjectWebApi(config);
        }
    }
}
