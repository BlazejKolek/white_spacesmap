﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using WhiteSpaceMap.Business.Interfaces;
using WhiteSpaceMap.Business.Models;

namespace WhiteSpaceMap.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var clients =  _clientService.GetAllClients();

            if (clients == null)
                return BadRequest($"Clients does not exist");

            return Ok(clients);
        }
            

    }
}
