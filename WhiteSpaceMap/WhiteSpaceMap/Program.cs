using System;
using Topshelf;

namespace WhiteSpaceMap.WebApi
{
    public class Program
    {
       private static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>
            {
                x.Service<OwinBootstrap>(s =>
                {
                    s.ConstructUsing(name => new OwinBootstrap());
                    s.WhenStarted(owinBootstrap => owinBootstrap.Start());
                    s.WhenStopped(owinBootstrap => owinBootstrap.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("White space WebApi ");
                x.SetDisplayName("WhiteSpaceWebApi");
                x.SetServiceName("WhiteSpaceWebApi");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
