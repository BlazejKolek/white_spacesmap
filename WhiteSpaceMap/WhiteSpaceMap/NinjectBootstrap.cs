﻿using Ninject;
using WhiteSpaceMap.Business;

namespace WhiteSpaceMap.WebApi
{
    public class NinjectBootstrap
    {
        public IKernel GetKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new BusinessModule());
            return kernel;
        }

    }
}
